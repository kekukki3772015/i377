package mvc.view;

import java.util.Map;

import mvc.model.Customer;

public class CustomerForm {
	
	private Customer customer;
	private Map<String, String> types;
	private boolean disabled;

	public Map<String, String> getTypes() {
		return types;
	}

	public void setTypes(Map<String, String> types) {
		this.types = types;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public boolean isDisabled() {
		return disabled;
	}
	
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	@Override
	public String toString() {
		return "customer: " + customer;
	}
	
}
