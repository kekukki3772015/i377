package mvc.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mvc.dao.CustomerDao;
import mvc.model.Customer;
import mvc.view.CustomerForm;

@Controller
public class CustomerController {

	@Resource
	private CustomerDao dao;

	@RequestMapping("/")
	public String home() {
		return "redirect:search";
	}
	
	@ResponseBody
	@RequestMapping("/test")
	public String test() {
		dao.saveCustomer(new Customer(21l, "Jane", "Doe", 123L, "customerType.private"));
		return "test";
	}

	@RequestMapping("/addForm")
	public String add(@ModelAttribute("customerForm") CustomerForm form) {
		form.setTypes(dao.getTypes());
		return "add";
	}

	@RequestMapping("/delete/{customerCode}")
	public String delete(@PathVariable("customerCode") Long customerCode) {
		dao.deleteCustomer(customerCode);
		return "redirect:/search";
	}

	@RequestMapping("/admin/{adminAction}")
	public String admin(@PathVariable("adminAction") String adminAction) {
		chooseAdminAction(adminAction);
		return "redirect:/search";
	}

	@RequestMapping("/search")
	public String search(HttpServletRequest request) {
		getCustomers(request);
		return "search";
	}

	@RequestMapping("/view/{customerCode}")
	public String view(@ModelAttribute("customerForm") CustomerForm form,
			@PathVariable("customerCode") Long customerCode) {
		form.setCustomer(dao.getCustomerByCode(customerCode));
		form.setTypes(dao.getTypes());
		form.setDisabled(true);
		return "add";
	}

	@RequestMapping("/save")
	public String save(@ModelAttribute("customerForm") CustomerForm form) {
		dao.saveCustomer(form.getCustomer());
		return "redirect:/search";
	}
	
	
	
	private void chooseAdminAction(String adminAction) {
		if (adminAction.equals("clearData"))
			dao.deleteCustomers();
		if (adminAction.equals("insertData"))
			dao.addExampleCustomers(dao);
	}
	
	private void getCustomers(HttpServletRequest request) {
		if (request.getParameter("searchString") != null) {
			if (request.getParameter("searchString") == "") {
				request.setAttribute("customers", dao.getCustomers());
			} else
				request.setAttribute("customers", dao.getFilteredCustomers(request.getParameter("searchString")));
		} else
			request.setAttribute("customers", dao.getCustomers());
	}

}
