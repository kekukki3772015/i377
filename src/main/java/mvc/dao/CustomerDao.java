package mvc.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mvc.model.Customer;

@Repository
public class CustomerDao {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Customer> getCustomers() {
		TypedQuery<Customer> query = em.createQuery("select c from Customer c", Customer.class);
		return query.getResultList();
	}

	@Transactional
	public void saveCustomer(Customer customer) {
		System.out.println(customer);
		if (customer.getId() == null) {
			em.persist(customer);
		} else {
			em.merge(customer);
		}
	}

	@Transactional
	public void deleteCustomer(Long code) {
		Query query = em.createQuery("delete from Customer c where c.code = :code");
		query.setParameter("code", code);
		query.executeUpdate();
	}

	@Transactional
	public List<Customer> getFilteredCustomers(String searchString) {
		TypedQuery<Customer> query = em.createQuery(
				"select c from Customer c where " + "upper(c.firstName) like :searchString or "
						+ "upper(c.surname) like :searchString or " + "cast((c.code) as string) like :searchString",
				Customer.class);
		query.setParameter("searchString", "%" + searchString.toUpperCase() + "%");
		return query.getResultList();
	}

	@Transactional
	public void deleteCustomers() {
		Query query = em.createQuery("delete from Customer");
		query.executeUpdate();
	}

	@Transactional
	public Customer getCustomerByCode(Long code) {
		TypedQuery<Customer> query = em.createQuery("select c from Customer c where c.code = :code", Customer.class);
		query.setParameter("code", code);
		return query.setMaxResults(1).getSingleResult();
	}

	public Map<String, String> getCustomerTypes() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("customerType.private", "Private");
		map.put("customerType.corporate", "Corporate");
		return map;
	}

	public Map<String, String> getTypes() {
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("", "");
		map.put("customerType.private", "Private");
		map.put("customerType.corporate", "Corporate");
		return map;
	}

	
	public void addExampleCustomers(CustomerDao dao) {
		//dao.saveCustomer(new Customer(1l, "Jane", "Doe", 123L, "customerType.private"));
		dao.saveCustomer(new Customer("Jane", "Doe", 123L, "customerType.private"));
		dao.saveCustomer(new Customer("Jane", "Doe", 456L, "customerType.corporate"));
		dao.saveCustomer(new Customer("John", "Smith", 789L, ""));
	}
}
