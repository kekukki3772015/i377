package mvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Customer {

	@Id
	@SequenceGenerator(name = "my_seq", sequenceName = "seq1", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
	private Long id;
	private String firstName;
    private String surname;
    private Long code;
    private String type;

    public Customer() {}
    
    public Customer(Long id, String firstName, String surname, Long code, String type) {
		this.setId(id);
		this.setFirstName(firstName);
		this.setSurname(surname);
		this.setCode(code);
		this.setType(type);
	}
    
    public Customer(String firstName, String surname, Long code, String type) {
        this.setFirstName(firstName);
        this.setSurname(surname);
        this.setCode(code);
        this.setType(type);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return id + " " + firstName  + " " + surname + " " + code + " " + type;
	}

}
