<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include page="header.jsp">
	<jsp:param name="pageTitle" value="Add" />
</jsp:include>

<c:if test="${not empty errors}">
	<div style="color: red">
		<c:forEach var="error" items="${errors}">
			<c:out value="${error}"></c:out>
			<br />
		</c:forEach>
	</div>

	<br />
	<br />

</c:if>

<c:url value="/save" var="theAction" />
<form:form method="post" action="${theAction}"
	modelAttribute="customerForm">
	<table class="formTable" id="formTable">
		<tbody>
			<tr>
				<td>Eesnimi:</td>
				<td><form:input name="firstName" id="firstNameBox"
						path="customer.firstName" disabled="${customerForm.disabled}" /></td>
			</tr>
			<tr>
				<td>Perekonnanimi:</td>
				<td><form:input name="surname" id="surnameBox"
						path="customer.surname" disabled="${customerForm.disabled}" /></td>
			</tr>
			<tr>
				<td>Kood:</td>
				<td><form:input name="code" id="codeBox" path="customer.code"
						disabled="${customerForm.disabled}" /></td>
			</tr>
			<tr>
				<td>T��p:</td>
				<td><form:select id="customerTypeSelect" name="customerType"
						path="customer.type" items="${customerForm.types}"
						disabled="${customerForm.disabled}" /></td>
			</tr>
			<c:choose>
				<c:when test="${customerForm.disabled}">
					<tr>
						<td colspan="2" align="right"><br> <a
							href="<c:url value="/search" />" id="backLink">Tagasi</a></td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="2" align="right"><br> <input type="submit"
							value="Lisa" id="addButton" /></td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</form:form>


<jsp:include page="footer.jsp" />