<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="header.jsp">
	<jsp:param name="pageTitle" value="View" />
</jsp:include>

<form method="post" action="/">
	<table class="formTable" id="formTable">
		<tbody>
			<tr>
				<td>Eesnimi:</td>
				<td><input name="firstName" value="<c:out value="${customer.firstName}"/>" id="firstNameBox"
					disabled="disabled"></td>
			</tr>
			<tr>
				<td>Perekonnanimi:</td>
				<td><input name="surname" value="<c:out value="${customer.surname}"/>" id="surnameBox"
					disabled="disabled"></td>
			</tr>
			<tr>
				<td>Kood:</td>
				<td><input name="code" value="<c:out value="${customer.code}"/>" id="codeBox"
					disabled="disabled"></td>
			</tr>
			<tr>
				<td>T��p:</td>
				<td>
					<select id="customerTypeSelect" name="customerType" disabled="disabled">
						<option value="">
						<c:forEach var="type" items="${types}">
							<c:set var="selected" value=""/>
					    	<c:if test="${type.key == customer.type}">
					    		<c:set var="selected" value="selected"/>
					    	</c:if>
							<option ${selected} value="<c:out value="${type.key}"/>"><c:out value="${type.value}"/></option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right"><br><a href="<c:url value="Search" />"
					id="backLink">Tagasi</a></td>
			</tr>
		</tbody>
	</table>
</form>

<jsp:include page="footer.jsp" />