<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="header.jsp">
    <jsp:param name="pageTitle" value="Search"/>
</jsp:include>

<form method="get" action="search">
	<input name="searchString" id="searchStringBox" value=""> <input
		type="submit" id="filterButton" value="Filtreeri"> <br/> <br/>
	<table class="listTable" id="listTable">
		<thead>
			<tr>
				<th scope="col">Nimi</th>
				<th scope="col">Perekonnanimi</th>
				<th scope="col">Kood</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="customer" items="${requestScope.customers}">
				<tr>
					<td>
						<div id="row_<c:out value="${customer.code}" />">
							<a href="view/<c:out value="${customer.code}" />" id="view_<c:out value="${customer.code}" />">
								<c:out value="${customer.firstName}" />
							</a>
						</div>
					</td>
					<td><c:out value="${customer.surname}" /></td>
					<td><c:out value="${customer.code}" /></td>
					<td><a href="delete/<c:out value="${customer.code}" />" id="delete_<c:out value="${customer.code}" />">Kustuta</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</form>

<jsp:include page="footer.jsp" />