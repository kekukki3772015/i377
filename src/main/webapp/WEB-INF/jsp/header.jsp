<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getParameter("pageTitle")%></title>
<%@ include file="styles.jsp"%>
</head>
<body>
	<ul id="menu">
		<li><a href="<c:url value="/search"/>" id="menu_Search">Otsi</a></li>
		<li><a href="<c:url value="/addForm"/>" id="menu_Add">Lisa</a></li>
		<li><a href="<c:url value="/admin/clearData"/>" id="menu_ClearData">Tühjenda</a></li>
		<li><a href="<c:url value="/admin/insertData"/>" id="menu_InsertData">Sisesta näidisandmed</a></li>
	</ul>
	<br>
	<br>
	<br>